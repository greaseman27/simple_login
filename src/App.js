import React, { Component } from 'react';
import Cookies from 'js-cookie';

import './styles/App.scss';
import server from './db/server';
import logo from './images/castleLogo.svg';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      successMsg: null,
      errorMsg: null,
      activeSession: false || this.checkSessionCookie(),
    }

    this.handleLogin = this.handleLogin.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }

  componentDidMount () {
    this.checkSessionCookie();
  }


  checkSessionCookie (name = 'activeSession') {
    const loggedInUser = Cookies.get(name);
    if (loggedInUser) {
      this.setState({
        email: loggedInUser,
        successMsg: 'Welcome',
        activeSession: true,
        errorMsg: null,
      });
    }
  }

  handleLogin () {
    const email = this.state.email;
    const password = this.state.password;

    //* Search db records for email key (b/c our 'server' is an object)
    const hasEmail = Object.keys(server).includes(this.state.email);
    let validPassword;

    //* if email key found, check for equality between 
    //* password in state, and password stored in db file
    if (hasEmail) {
      validPassword = server[email] === password;
    } else {
      this.setState({ errorMsg: 'User not found.' })
    }

    //* if passwords match
    //* login => set session cookie.
    //* display success message.
    if (validPassword) {
      Cookies.set('activeSession', `${email}`);
      this.setState({
        successMsg: 'Welcome',
        activeSession: true,
        errorMsg: null,
      });
    }
    //* Display error message
    else {
      this.setState({ errorMsg: 'User not found' });
    }

  }

  handleLogout () {
    document.cookie = 'activeSession=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
    this.setState({
      activeSession: false,
      email: null,
      password: null,
    });
  }

  render () {
    const state = this.state;
    const isLoggedIn = state.activeSession;
    const hasLoginError = state.errorMsg;

    return (
      <div className="form-wrapper">

        <div className="ui container login-form">
          <header>
            <img src={logo} alt="" />
          </header>
          {
            isLoggedIn &&
            <h3>{state.successMsg}, {state.email}!</h3>
          }

          {
            hasLoginError &&
            <h3>{this.state.errorMsg}</h3>
          }

          {
            !isLoggedIn &&
            <React.Fragment>
              {
                !hasLoginError &&
                <h3>Please log in</h3>
              }
              <div className="ui input large">
                <input
                  type="text"
                  placeholder="Email address"
                  onChange={(e) => this.setState({ email: e.target.value })} />
              </div>
              <div className="ui input large">
                <input
                  type="password"
                  placeholder="Password"
                  onChange={(e) => this.setState({ password: e.target.value })} />
              </div>
            </React.Fragment>
          }
          {
            !isLoggedIn &&
            <button
              onClick={this.handleLogin}
              className="ui primary button">Sign In</button>
          }

          {
            isLoggedIn &&
            <button
              onClick={this.handleLogout}
              className="ui primary button">Sign Out</button>
          }
        </div>
        <footer className="copyright">© 2019</footer>
      </div>
    );
  }
}

export default App;