
## Available Scripts

After cloning this repo:
1. `npm install` to install the necessary dependencies.
2. `npm start` Runs the app in the development mode.

Once the dev server finishes, you can navigate to [http://localhost:3000](http://localhost:3000) to view the project in the browser.

Cheers!

